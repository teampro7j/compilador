#Compilador
Este proyecto es un compilador que traduce de un lenguaje .pro a un .cpp
El nombre del compilador es PsyPro, esta creado en c++ y recibe dos pparametros por medio de la consola, el primero es la ruta del archivo a compilar y el segundo es el nombre del archivo en si. 
El compilador se puede mandar a llamar por medio del editor de texto que se tiene en este proyecto, el archivo que se esta editando es el que se compila y el archivo .cpp que se genera se crea en la misma ruta donde se encuentra el archivo que se esta editando

##Sintaxis
###Caracteristicas
- Es sensible a mayusculas
- Los comentarios se realizan por medio de `//...` para una sola línea y `/! ... !/` para comentarios en multiples líneas
- Todas las instrucciones de control deben llevar llaves al inicio y al final `{...}`
- Las sentencias deben terminar con punto y coma `;`
- La asignación se realizará con el operador `=`
- ####Tipos de datos
    - `int` en el que se pueden tener números enteros
    - `float` que pueden ser números con punto decimal
    - `string` que es una cadena de caracteres
    - `bool` que puede ser `true` o `false`
- ####Operadores aritmeticos
    - `+` para suma
    - `-` para resta
    - `*` para multiplicación 
    - `/` para división 
- ####Operadores logicos
    - `==` igual a 
    - `!=` no igual a
    - `<` menor que
    - `>` mayor que
    - `<=` menor o igual que
    - `>=` mayor o igual que
    - `!` negación
###Entrada de datos
```
read <-- var
```
###Salida de datos
```
write --> var
```
###Condiciones
```
when(var1 == var2) {
    *sentencias...*
}
instead {
    *sentencias*
}
```
###Ciclos
```
until(condition) {
   *sentencias*
}
```

```
realize {
   *sentencias*
} until(condition);
```

```
repeat(inicialization, condition, increment) {
    *sentencias*
}
```
###Funciones
```
Tipo_De_Dato Nombre_Funcion(Tipo_De_Dato Parametro1, Tipo_De_Dato Parametro2, ...) {
    *sentencias*
    back Dato;
}
```
```
int suma(int a, int b) {
   back a+b;
}
```
##Colaboradores
* Juan José Carvajal Muñoz 			13300050
* Adrián Alejádro Dávalos Sánchez	13300075
* José Miguel Padilla Villalvazo	13300256