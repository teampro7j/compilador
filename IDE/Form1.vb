﻿Public Class Form1

    Private currentFile As String
    Private saved As Boolean
    Private currentName As String
    Private saveCount As Integer

    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        Me.saved = True
        Me.saveCount = 1
    End Sub





    'Para preguntar si guardar los cambios actuales
    Private Function ask() As Boolean
        Dim result As MsgBoxResult
        If Me.saved = False Then
            result = MsgBox("¿Desea guardar el archivo antes de cerrarlo?", MsgBoxStyle.YesNoCancel, "Cerrar")
        End If
        Select Case result
            Case MsgBoxResult.Yes
                GuardarToolStripMenuItem.PerformClick()
                Return False
            Case MsgBoxResult.No
                Return False
            Case MsgBoxResult.Cancel
                Return True
            Case Else
                Return True

        End Select
    End Function

    'Archivo
    'nuevo
    'TERMINADO
    Private Sub NuevoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NuevoToolStripMenuItem.Click
        Dim cancel As Boolean
        If Me.saved = False Then
            cancel = Me.ask()
        End If

        If cancel = False Then
            txt_edit.Visible = True
            txt_edit.Text = ""
            Me.currentFile = ""
            Me.Text = "Sin nombre - Editor"
            Me.currentName = "Sin nombre - Editor"
            'Me.saved = False
            Me.saveCount = 1
        End If

    End Sub
    'abrir
    'TERMINADO
    Private Sub AbrirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AbrirToolStripMenuItem.Click
        Dim cancel As Boolean
        If Me.saved = False Then
            cancel = Me.ask()
        End If

        If cancel = False Then
            Dim fileName As String
            OpenFileDialog1.InitialDirectory = "C:\Users\INFORMATICA\Desktop"
            OpenFileDialog1.Filter = "Text files(*.txt)|*.pro"
            OpenFileDialog1.Multiselect = False
            If OpenFileDialog1.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
                fileName = OpenFileDialog1.FileName
                currentFile = fileName
                Me.saved = True
                txt_edit.Visible = True
            End If
            If System.IO.File.Exists(fileName) = True Then
                Me.Text = OpenFileDialog1.SafeFileName + " - Editor"
                Dim objReader As New System.IO.StreamReader(fileName)
                txt_edit.Text = objReader.ReadToEnd
                objReader.Close()
                Me.Text = OpenFileDialog1.SafeFileName
                Me.currentName = Me.Text
                Me.saveCount = 1
                Me.saved = True
            End If
        End If
    End Sub
    'cerrar
    'TERMINADO
    Private Sub CerrarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CerrarToolStripMenuItem.Click
        Dim cancel As Boolean
        If Me.saved = False Then
            cancel = Me.ask()
        End If
        If cancel = False Then
            Me.currentFile = ""
            txt_edit.Visible = False
            txt_edit.Text = ""
            Me.Text = "Editor"
            Me.saved = True
        End If


    End Sub
    'salir
    'TERMINADO
    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        If Me.saved = False Then
            Dim result As MsgBoxResult
            result = MsgBox("¿Desea guardar el archivo antes de salir?", MsgBoxStyle.YesNoCancel, "Salir")
            If result = MsgBoxResult.Yes Then
                If Me.currentFile <> "" Then
                    My.Computer.FileSystem.WriteAllText _
                    (Me.currentFile, txt_edit.Text, False)
                Else
                    If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK _
            Then
                        My.Computer.FileSystem.WriteAllText _
                (SaveFileDialog1.FileName, txt_edit.Text, False)

                    End If
                End If

            ElseIf result = MsgBoxResult.No Then
                Me.Close()
            End If
        End If
        Me.Close()
    End Sub
    'guardar
    'TERMINADO
    Private Sub GuardarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GuardarToolStripMenuItem.Click
        SaveFileDialog1.Filter = "TXT Files (*.txt)|*.pro"
        If Me.currentFile <> "" Then
            My.Computer.FileSystem.WriteAllText _
            (Me.currentFile, txt_edit.Text, False)
            Me.saved = True
            Me.Text = System.IO.Path.GetFileName(SaveFileDialog1.FileName) + " - Editor"
            Me.saveCount = 1
        Else
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK _
            Then
                My.Computer.FileSystem.WriteAllText _
                (SaveFileDialog1.FileName, txt_edit.Text, False)
                Me.currentFile = SaveFileDialog1.FileName
                Me.saved = True
                Me.Text = System.IO.Path.GetFileName(SaveFileDialog1.FileName) + " - Editor"
                Me.saveCount = 1
            End If
        End If
    End Sub
    'guardar como
    'TERMINADO
    Private Sub GuardarComoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GuardarComoToolStripMenuItem.Click
        SaveFileDialog1.Filter = "Pro files (*.pro)|*.pro"
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK _
        Then
            My.Computer.FileSystem.WriteAllText _
            (SaveFileDialog1.FileName, txt_edit.Text, False)
            Me.currentFile = SaveFileDialog1.FileName
            Me.Text = System.IO.Path.GetFileName(SaveFileDialog1.FileName) + " - Editor"
            Me.saved = True
            Me.saveCount = 1
        End If
    End Sub
    'shell
    'TERMINADO
    Private Sub ShellToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ShellToolStripMenuItem.Click
        If System.IO.File.Exists("interprete.exe") Then
            Process.Start("interprete.exe")
        Else
            MessageBox.Show("No se encuentra el shell")
        End If
    End Sub




    'EDICION
    'copiar
    'TERMINADO
    Private Sub CopiarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CopiarToolStripMenuItem.Click
        SendKeys.Send("^c")
    End Sub
    'cortar
    'TERMINADO
    Private Sub CortarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CortarToolStripMenuItem.Click
        SendKeys.Send("^x")
    End Sub
    'pegar
    'TERMINADO
    Private Sub PegaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PegaToolStripMenuItem.Click
        SendKeys.Send("^v")
    End Sub




    'TERMINADO
    Private Sub txt_edit_TextChanged(sender As Object, e As EventArgs) Handles txt_edit.TextChanged

        If Me.saveCount < 2 Then
            Me.saveCount += 1
        End If
        If Me.saveCount = 2 Then
            If Me.Text(Me.Text.Length - 1) <> "*" Then
                Me.Text = Me.currentName + "*"
            End If
            Me.saved = False
        End If
    End Sub

    'TERMINADO
    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If Me.saved = False Then
            Dim result As MsgBoxResult
            result = MsgBox("¿Desea guardar el archivo antes de salir?", MsgBoxStyle.YesNoCancel, "Salir")
            If result = MsgBoxResult.Yes Then
                If Me.currentFile <> "" Then
                    My.Computer.FileSystem.WriteAllText _
                    (Me.currentFile, txt_edit.Text, False)
                Else
                    If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK _
            Then
                        My.Computer.FileSystem.WriteAllText _
                (SaveFileDialog1.FileName, txt_edit.Text, False)

                    End If
                End If


            ElseIf result = MsgBoxResult.Cancel Then
                e.Cancel = True
            End If
        End If
    End Sub
    'TERMINADO
    Private Sub AcercaDeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AcercaDeToolStripMenuItem.Click
        Dim about As New Acerca
        about.Show()
    End Sub

    Private Sub AyudaToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles AyudaToolStripMenuItem1.Click
        If System.IO.File.Exists("_tmphtm/nuevo.htm") Then
            Process.Start("_tmphtm\nuevo.htm")
        Else
            MessageBox.Show("Faltan archivos de ayuda")
        End If
    End Sub



    'Compilar
    Private Sub CompilarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CompilarToolStripMenuItem.Click
        GuardarToolStripMenuItem_Click(sender, e)
        Dim path = System.IO.Path.GetDirectoryName(Me.currentFile)
        Dim file = System.IO.Path.GetFileName(Me.currentFile)
        If System.IO.File.Exists("psypro.exe") Then
            Dim p As New ProcessStartInfo
            p.FileName = "psypro.exe"
            p.Arguments = path & " " & file
            Process.Start(p)
        Else
            MessageBox.Show("Compilador no encontrado")
        End If
        System.Threading.Thread.Sleep(1000)
        If System.IO.File.Exists(path & "\error.txt") Then
            MessageBox.Show(My.Computer.FileSystem.ReadAllText(path & "\error.txt"))
        Else
            MessageBox.Show("Compilacion terminada con exito")
        End If


    End Sub

End Class
