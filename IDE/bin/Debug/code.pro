﻿/!ejemplo de un programa hecho
en lenguaje .pro
cosas basicas, asignaciones, ciclos, condiciones
leer y escribir !/

int a;
int b;

int add(int a, int b) {
	back a + b;
}

int main() {
	read <-- a; //leer a
	read <-- b; //leer b
	/!aqui se hacen 
	unas comparaciones
	para ver cual es mayor
	!/

	when(a == b) {
		write --> "A es igual que B";
	} instead when (a > b) {
		write --> "A es mayor que B";
	} instead {
		write --> "B es mayor que A";
	}
	//ciclo "While"
	until(a > 0) {
		write --> a;
		a--;
	}
	//ciclo "for"
	repeat(int i = 0; i < b; i++) {
		write --> i;
	}
	int suma = add(a,b);
	write --> suma;
}