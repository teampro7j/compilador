/*********************
**********************

    PsyPro - Compilador para el lenguaje .pro
    Creadores:
    Copyright:
    Fecha de Creacion:

**********************
**********************/

//#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <vector>
#include <string.h>
#include <sstream>

using namespace std;


vector<string> textArray;
vector<string> tokens;
vector<string> simbols;
vector<string> variables;
string error;
int a;
string texto;
string allText;
string path, fileName ,currentFile;
string finalResult;
string libs = "#include <iostream> \n#include <string> \n#include <algorithm> \n#include <vector> \n#include <string.h> \nusing namespace std;\n";

//fill the table
void fillSimbols() {
    simbols.push_back("int");
    simbols.push_back("float");
    simbols.push_back("string");
    simbols.push_back("bool");
    simbols.push_back("read");
    simbols.push_back("write");
    simbols.push_back("when");
    simbols.push_back("instead");
    simbols.push_back("until");
    simbols.push_back("realize");
    simbols.push_back("repeat");
    simbols.push_back("back");
}

//checks if the tok is on the list of simbols
bool onList(string tok, vector<string> arr) {
    for(int i = 0; i < arr.size(); i++) {
        if(tok == arr[i]) return true;
    }
    return false;
}

//split string
vector<string> split(string str, char delimiter) {
  vector<string> internal;
  stringstream ss(str); // Turn the string into a stream.
  string tok;

  while(getline(ss, tok, delimiter)) {
    internal.push_back(tok);
  }

  return internal;
}

//delete tabs and consecutive spaces
string eliminarTabs(string my_str) {
    string str2;
    int p = 0;
	//delete tabs
    for (int i = 0; i < my_str.size(); i++) {
		if (my_str[i] == '\t') {
			my_str[i] = ' ';
			i++;
			while (my_str[i] == '\t' || my_str[i] == ' ') {
				my_str.erase(my_str.begin()+i);
			}
		}
	}

	//delete consecutive spaces
    while(!my_str.empty() && isspace(*my_str.begin()))
        my_str.erase(my_str.begin());

    while(!my_str.empty() && isspace(*my_str.rbegin()))
        my_str.erase(my_str.length()-1);

	return my_str;
}

//delete all kind of comments
vector<string> deleteComments(vector<string>cad) {
	bool isMultiLine = false;
	int startJ,startI,rows = 1,w = 1;
	for (int i = 0; i < cad.size(); i++) {
		rows = 1;
		isMultiLine = false;
		for (int j = 0; j < cad[i].size();j++) {
			//Delete Single line comment
			if (cad[i][j] == '/' && cad[i][j+1] == '/') {
				cad[i].erase(cad[i].begin()+j,cad[i].end());
				break;
			}
			//Delete Multi-Line comment
			if (cad[i][j] == '/' && cad[i][j+1] == '!') {
                startJ = j;
                startI = i;
                j+=2;
				while (cad[i][j] != '!' || cad[i][j+1] != '/') {
					j++;
					if(j >= cad[i].size())
					{
						i++;
						j = 0;
						rows++;
						isMultiLine = true;
					}
					if(i >= cad.size()) {
                        error = "comment not ended";
                        return cad;
					}
				}

				//Multi-Line
                if (isMultiLine) {
					cad[startI].erase(cad[startI].begin()+startJ,cad[startI].end());
					for (w = 1; w < rows-1; w++) {
						cad[startI+w].erase(cad[startI+w].begin(),cad[startI+w].end());
					}
					i = w;
					if (j < cad[startI+i].size()) cad[startI+i].erase(cad[startI+i].begin(),cad[startI+i].begin()+j+2);
					else cad[startI+i].erase(cad[startI+i].begin(),cad[startI+i].end());
				}

				//Not Multi-Line
				else if (j < cad[i].size()) cad[i].erase(cad[i].begin()+startJ,cad[i].begin()+j+2);
				else cad[i].erase(cad[i].begin()+startJ,cad[i].begin()+j);
				j = startJ;
				i = startI;

			}
		}
	}
	return cad;
}

/*count Parentesis Llaves y Corchetes, tambien elimina las comillas para no contar los strings
el numero de '(', '{', '[' debe ser igual al numero de ')', '}', ']'
en caso de ser positivo significa que hay mas de apertura que de cierre
en caso de ser negativo significa que hay mas de cierre que de apertura
*/
void countPLC(string text) {
    int startString, endString;
    int parentesis = 0;
    int corchetes = 0;
    int llaves = 0;
    int sc = 0;
    while(sc < text.size()-1) {
    if(text[sc] == '\"') {
            startString = sc;
            sc++;
            while(text[sc] != '\"' && sc < text.size()-1) sc++;
            if(text[sc] == '\"') {
                endString = sc;
                text.erase(text.begin()+startString,text.begin()+endString+1);
            } else {
                error = "No se encontro el fin de una cadena de texto";

                //return;
            }
        }
        sc++;
    }

    for(int i = 0; i < text.size(); i++) {
        //falta eliminar cuando esten dentro de comillas simples o dobles
        if(text[i] != '\"' || text[i] != '\'') {
            switch(text[i]) {
            case '(':
                parentesis++;
                break;
            case ')':
                parentesis--;
                break;
            case '[':
                corchetes++;
                break;
            case ']':
                corchetes--;
                break;
            case '{':
                llaves++;
                break;
            case '}':
                llaves--;
                break;
            default:
                break;
            }
        }

    }
    if(parentesis > 0)
        error = "Falta un )";
    if(parentesis < 0)
        error = "Falta un (";

    if(corchetes > 0)
        error = "Falta un ]";
    if(corchetes < 0)
        error = "Falta un [";

    if(llaves > 0)
        error = "Falta un }";
    if(llaves < 0)
        error = "Falta un {";

}


void analizateToken(string token) {
    

}



string replaceAll(string str, const string& from, const string& to) {
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
    }
    return str;
}

string parseAll(string parseCad) {
	parseCad = replaceAll(parseCad,"read","cin");
	parseCad = replaceAll(parseCad,"<--",">>");
	parseCad = replaceAll(parseCad,"write","cout");
	parseCad = replaceAll(parseCad,"-->","<<");
	parseCad = replaceAll(parseCad,"when","if");
	parseCad = replaceAll(parseCad,"instead","else");
	parseCad = replaceAll(parseCad,"until","while");
	parseCad = replaceAll(parseCad,"repeat","for");
	parseCad = replaceAll(parseCad,"back","return");
	parseCad = replaceAll(parseCad,"realize","do");
	return parseCad;
}

string changeExtension(string cad) {
    cad = replaceAll(cad,".pro",".cpp");
    return cad;
}

void makeError() {
    fileName = "error.txt";
    currentFile = path + fileName;
    const char *ff = currentFile.c_str();
    ofstream fs(ff);
    fs << error;
    fs.close();
}

void saveFile() {
    fileName = changeExtension(fileName);
    string errorFile = path + "\\error.txt";
    const char *rf = errorFile.c_str();
    currentFile = path + fileName;
    //Delete error file
    remove(rf);
    const char *ff = currentFile.c_str();
    ofstream fs(ff);
    fs << allText;
    fs.close();
}

int main(int argc, char* argv[])
{
    fillSimbols();

	path = argv[1];
	path += "\\";
	fileName = argv[2];
	currentFile = path+fileName;
	const char *cf = currentFile.c_str();
    ifstream archivo(cf);
	int i = 0;
	if (archivo.is_open()) {
		while (getline(archivo, texto)) {
            //lexico
			texto = eliminarTabs(texto);
			if(texto[0] == ' ') texto.erase(texto.begin());
			if(i == 0) {
                //por alguna razon los primeros 3 caracteres son basura
                texto.erase(texto.begin(),texto.begin()+3);
                i++;
			}
			textArray.push_back(texto);
		}
		textArray = deleteComments(textArray);
		archivo.close();
		if(error != "") {
            //write it on the error file and end
            makeError();
            return 0;
		} else {
			for (int i = 0;i < textArray.size();i++) {
                if (!textArray[i].empty()) {
                    allText += textArray[i] + " ";
                }
            }
            //sintactico
            countPLC(allText);
            if(error == ""){
                //no errors, everything its ok
                //get tokens
                char *dup = strdup(allText.c_str());
                char *p = strtok(dup,";{}");
                while(p) {
                         if(p != "")
                              tokens.push_back(p);
                         p = strtok(NULL,";{}");
                }
                for(int i = 0; i < tokens.size()-2; i++) {
                    if(tokens[i][0] == 32) tokens[i].erase(tokens[i].begin());
                    analizateToken(tokens[i]);
                }
                if(error == "") {
                   //all done, parse the text
                    allText = libs + allText;
                    allText = parseAll(allText);

                    //write the final cpp file
                    saveFile();
                } else {
                    makeError();
                    return 0;
                }

            }
            else {
                //write it on the error file and end
                makeError();
                return 0;
            }
		}

	}
	else {
		cout << "No fue posible abrir el archivo" << endl;
	}
	return 0;
}
